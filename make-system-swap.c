/*
 * Make a system slow to respond by causing it to swap a lot.  Useful
 * to test programs and shake out bugs that are unintentionally
 * time-sensitive.
 *
 * Copyright 2012 Red Hat, Inc.
 * Copyright 2012 Amit Shah <amit.shah@redhat.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  COPYING file for the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <error.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

bool debug;

struct mem {
	void *addr;
	unsigned long long len;
};

struct mem alloc_mem;   /* All of the allocated area */

struct thread_info {
	struct mem mem;
	pthread_t thread;
};

struct thread_info *thread_info;
int nr_threads;

void signal_handler(int signal)
{
	unsigned int i, threads;

	threads = nr_threads;
	nr_threads = 0;
	for (i = 0; i < threads; i++) {
		if (thread_info[i].mem.len) {
			pthread_cancel(thread_info[i].thread);
		}
	}
	for (i = 0; i < threads; i++) {
		if (thread_info[i].mem.len) {
			pthread_join(thread_info[i].thread, NULL);
		}
	}
	free(thread_info);
	free(alloc_mem.addr);
	exit(EXIT_SUCCESS);
}

unsigned long long get_total_mem(void)
{
	FILE *file;
	char type[50];
	char scale[5];
	unsigned long long total_mem;
	int ret;

	file = fopen("/proc/meminfo", "r");
	if (!file) {
		error(-ENOMEM, errno, "Error opening /proc/meminfo");
	}

	do {
		ret = fscanf(file, "%s %llu %s\n", type, &total_mem, scale);
		if (ret == EOF) {
			error(-EINVAL, 0, "No total meminfo found");
		}
	} while (strncmp(type, "MemTotal", 8));
	fclose(file);

	/*
	 * TODO: Convert to GB, MB, etc., here.  However,
	 * /proc/meminfo doesn't show those values yet.
	 */
	if (!strncmp(scale, "kB", 2)) {
		/* Convert to bytes */
		total_mem <<= 10;
	}
	return total_mem;
}

void touch_all_mem(struct mem *mem)
{
	int *addr;
	unsigned long long i;

	addr = mem->addr;
	for (i = 0; i < mem->len / sizeof(int); i += sizeof(int)) {
		if (!addr) {
			/* Dry-run mode */
			continue;
		}
		if (!nr_threads) {
			/* Program received a kill signal */
			break;
		}

		addr[i] = 0x01010101;
	}
}

void* do_work(void *arg)
{
	struct mem *mem;

	mem = arg;

	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

	/* Touch everything once */
	touch_all_mem(mem);

	if (debug) {
		fprintf(stderr, "touched all pages\n");
	}

	while (nr_threads) {
		/*
		 * FIXME: Randomly touch pages instead of everything
		 * sequentially
		 */
		touch_all_mem(mem);
	}
	return NULL;
}

void show_usage(const char *name)
{
	fprintf(stderr, "Usage: %s [-r] [-m <size>] [-e <size>] [-t threads] "
			"[-d]\n", name);
	fprintf(stderr, "\t-r:\tRun the program.  Default is to not allocate memory.\n");
	fprintf(stderr, "\t-m:\tAllocate <size> MB of memory.  Default: all available.\n");
	fprintf(stderr, "\t-e:\tExclude (do not allocate) <size> MB memory.\n");
	fprintf(stderr, "\t-t:\tNumber of threads to spawn.  Default 1\n");
	fprintf(stderr, "\t-d:\tPrint debug information\n");
}

/*
 * 1. Get amount of memory in the system
 * 2. Allocate that much memory
 * 3. Touch each page that got allocated by writing garbage till stopped
 * 4. TODO: Randomly keep touching pages till stopped
 */
int main(int argc, char **argv)
{
	struct sigaction sa;
	char *endptr;
	unsigned long long total_mem;
	unsigned long long exclude_mem;
	unsigned int i;
	int ret, opt;
	bool dry_run;

	dry_run = 1;
	debug = 0;

	total_mem = get_total_mem();
	alloc_mem.len = total_mem;
	exclude_mem = 0;

	nr_threads = 1;

	while ((opt = getopt(argc, argv, "rm:e:t:d")) != -1) {
		switch(opt) {
		case 'r':
			dry_run = 0;
			break;
		case 'm':
			alloc_mem.len = strtoull(optarg, &endptr, 10);
			if (!alloc_mem.len) {
				error(-EINVAL, 0, "Invalid size for -m");
			}
			if (optarg && *endptr != '\0') {
				error(-EINVAL, 0, "Invalid size for -m");
			}
			alloc_mem.len <<= 20;
			break;
		case 'e':
			exclude_mem = strtoull(optarg, &endptr, 10);
			if (!exclude_mem) {
				error(-EINVAL, 0, "Invalid size for -e");
			}
			if (optarg && *endptr != '\0') {
				error(-EINVAL, 0, "Invalid size for -e");
			}
			exclude_mem <<= 20;
			break;
		case 't':
			nr_threads = strtol(optarg, &endptr, 10);
			if (!nr_threads) {
				error(-EINVAL, 0, "Invalid number for -t");
			}
			if (optarg && *endptr != '\0') {
				error(-EINVAL, 0, "Invalid number for -t");
			}
			break;
		case 'd':
			debug = 1;
			break;
		default:
			show_usage(argv[0]);
			exit(EXIT_FAILURE);
		}
	}

	if (exclude_mem >= alloc_mem.len) {
		error(-EINVAL, 0, "Can't ignore more memory than available.");
	}

	alloc_mem.len -= exclude_mem;

	if (!alloc_mem.len) {
		error(-EINVAL, 0, "No memory to be allocated?");
	}

	/* FIXME: Also check if there are too many threads */
	if (nr_threads < 1) {
		error(-EINVAL, 0, "Invalid thread count specified");
	}

	if (dry_run) {
		printf("Dry-run: Not really allocating any memory.\n");
	}
	printf("System has %llu bytes of RAM.\n", total_mem);
	printf("Allocating %llu bytes.\n", alloc_mem.len);

	sa.sa_handler = signal_handler;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;

	ret = sigaction(SIGINT, &sa, NULL);
	if (ret) {
		error(-EINVAL, 0, "Could not install signal handler");
	}

	alloc_mem.addr = NULL;
	if (!dry_run) {
		alloc_mem.addr = malloc(alloc_mem.len);
		if (!alloc_mem.addr) {
			error(-ENOMEM, 0, "Can't allocate memory");
		}
	}

	if (debug) {
		fprintf(stderr, "alloc_mem addr is %p, len is %llu\n",
			alloc_mem.addr, alloc_mem.len);
	}

	thread_info = malloc(sizeof(struct thread_info) * nr_threads);

	for (i = 0; i < nr_threads; i++) {
		thread_info[i].mem.len = alloc_mem.len / nr_threads;

		thread_info[i].mem.addr = NULL;
		if (!dry_run) {
			thread_info[i].mem.addr = alloc_mem.addr
						 + (i * thread_info[i].mem.len);
		}

		if (debug) {
			fprintf(stderr, "thread %d len = %llu, addr = %p\n",
				i, thread_info[i].mem.len,
				thread_info[i].mem.addr);
		}

		ret = pthread_create(&thread_info[i].thread, NULL, do_work,
				     &thread_info[i].mem);
		if (ret) {
			fprintf(stderr, "Error creating thread %d\n", i);
			thread_info[i].mem.len = 0;
		}
		if (debug) {
			fprintf(stderr, "created thread %u\n", i);
		}
	}

	for (i = 0; i < nr_threads; i++) {
		if (thread_info[i].mem.len) {
			pthread_join(thread_info[i].thread, NULL);
		}
	}

	free(thread_info);
	free(alloc_mem.addr);
	return 0;
}
