CFLAGS = -g -Wall

make-system-swap: make-system-swap.c
	gcc $(CFLAGS) -lpthread -o $@ $<

all: make-system-swap

clean:
	rm make-system-swap
